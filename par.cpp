#include <string>
#include <boost/mpi.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/access.hpp>
#include <boost/mpi/timer.hpp>
#include <iostream>
#include <sstream>
#include <fstream>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <stack>
#include <vector>

#include "State.h"
#include "Message.h"
#include "Process.h"

#define LOG_FILE "/mnt/data/stefaja5/logs/log_par.txt"

int main(int argc, char **argv) {

	Process p;

	double t1, t2, loadTime, execTime;
	
	p.world.barrier();

	if (p.world.rank() == 0) {
		t1 = p.timer.elapsed();
	}

	/* nacteni vstupnich dat */
	if (!p.loadData(argv)) {
		std::cerr << "Pri nacitani dat ze souboru doslo k chybe" << std::endl;
		return 1;
	}
	
	p.world.barrier();

	if (p.world.rank() == 0) {
		t2 = p.timer.elapsed();
		loadTime = t2 - t1;
	}

	/* vypocet */
	std::string result = p.parallelLCS();

	p.world.barrier();

	if (p.world.rank() == 0) {	
		t1 = p.timer.elapsed();
		execTime = t1 - t2;
		p.printResult(result, LOG_FILE, argv[1], loadTime, execTime);
	}

	std::cout << "Procesor: " << p.world.rank() << " provedl " << p.expCtr << " expanzi." << std::endl;

	return 0;
}
