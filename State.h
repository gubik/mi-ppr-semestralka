#ifndef STATE_H
#define STATE_H

/*
 * Trida, ktera reprezentuje jeden stav v prohledavani stavoveho prostoru.
 * Obsahuje informaci o bitu pridavanem do retezce(bit) a pozici, na kterou se bit pridava(index).
*/

class State {
	std::string m_Bit;
	int m_Index;

public:
	State(const std::string &bit, const int &index) {
		m_Bit = bit;
		m_Index = index;
	};

	State() {
		m_Bit = std::string("x");
		m_Index = -1;

	};

	std::string getBit() { 
		return m_Bit; 
	};

	int getIndex() { 
		return m_Index; 
	};
protected:
	/* Serializace pro boost */

	friend class boost::serialization::access;
	
	template<class Archive>
	void serialize(Archive& ar, const unsigned int version) {
		ar & m_Bit & m_Index;
	};
};

#endif