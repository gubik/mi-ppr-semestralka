#include <iostream>
#include <sstream>
#include <fstream>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <sys/time.h>
#include <stack>

#include "State_seq.h"

#define LOG_FILE "/mnt/data/stefaja5/logs/log.txt" // cesta k souboru s logy

using namespace std;

typedef unsigned long long uint64;

uint64 startTime;

/* Returns the amount of milliseconds elapsed since the UNIX epoch.*/
uint64 GetTimeMs64()
{
	struct timeval tv;

	gettimeofday(&tv, NULL);

	uint64 ret = tv.tv_usec;
	/* Convert from micro seconds (10^-6) to milliseconds (10^-3) */
	ret /= 1000;

	/* Adds the seconds (10^0) after converting them to milliseconds (10^-3) */
	ret += (tv.tv_sec * 1000);

	return ret;
}

/* Konverze string -> int */
int str2int(const string &str) {

	stringstream ss(str);
	int num;

	if ((ss >> num).fail()) { 
		return -1;
	}

	return num;
}

/* Najde delku nejkratsiho retezce */
int getMinStringLength(const int &N, const int &Z, string * &inputStrings) {
	int min = Z;
	for (int i = 0; i < N; i++) {
		if (inputStrings[i].length() < min) {
			min = inputStrings[i].length();
		}
	}
	return min;
}

/* Nacte vstupni data ze souboru */
bool loadData(int &N, int &Z, int &K, string * &inputStrings, const string &inputData) {

	string line; // jeden radek soubor
	int lineCtr = 0; // citac radku

	ifstream ifs;
	ifs.open(inputData.c_str());

	while(!ifs.eof()) {
		getline(ifs, line);
		
		if (lineCtr == 0) {
			// prvni radek (N)
			N = str2int(line);
			if (N < 10) {
				cout << "N nesmi byt mensi nez 10." << endl;
				return false;
			}
			inputStrings = new string[N];
		} else if (lineCtr == 1) {
			// druhy radek (Z)
			Z = str2int(line);
			if (Z <= log(N)/log(2)) {
				cout << "Z musi byt vetsi nez log_2(" << N << ") = " << log(N)/log(2) << endl;
				return false;
			}
		} else if (lineCtr == N + 2) {
			// posledni radek (K)
			K = str2int(line);
			int minLength = getMinStringLength(N, Z, inputStrings);
			if (K <= minLength/2 || K >= minLength) {
				cout << "K musi byt v intervalu (" << minLength/2 << ", " << minLength << ")." << endl;
				return false;
			}
		} else {
			// naplneni pole retezcu
			inputStrings[lineCtr - 2] = line;
		}
		lineCtr++;
	}

	ifs.close();

	return true;
}

/* Vypis vsech retezcu */
void printStrings(string * &inputStrings, const int &N, ofstream &out) {

	for (int i = 0; i < N; i++) {
		out << inputStrings[i] << endl;
	}
}

/* Kontroluje, zda je string podposloupnosti jineho stringu */
bool isSubsequence(const string &subSequence, const string &input) {
	int pos = 0;

	for (int i = 0; i < input.length(); i++) {
		if (input[i] == subSequence[pos]) {
			pos++;
		}
		if (subSequence.length() == pos) {
			return true;
		}
	}
	return false;
}

bool checkAllStrings(string * &inputStrings, const string &subSequence, const int &N) {
	for (int i = 0; i < N; i++) {
		if (!isSubsequence(subSequence, inputStrings[i])) {
			return false;
		}
	}
	return true;
}


/* Zjisti nejdelsi spolecnou podposloupnost mnoziny retezcu */
string lcs(string * &inputStrings, const int &N, const int &Z, const int &K, int &expCtr) {

	// inicializace zasobniku
	stack<State> st;

	string candidate = ""; // zpracovavany retezec v kazdem stavu
	string result = ""; // dosud nejlepsi nalezene reseni 

	// naplneni zasobniku pocatecnimi hodnotami
	st.push(State("0", 0));
	st.push(State("1", 0));

	// dokud je neco na zasobniku
	while(!st.empty()) {
		// precti vrchol zasobniku
		State item = st.top();
		// odstran vrchol zasobniku
		st.pop();

		expCtr++;

		// orezani zpracovavaneho retezce podle pozice stavu ve stromu
		candidate.resize(item.getIndex());
		// pridani nasledujiciho bitu
		candidate += item.getBit();

		bool subSequence = true; // zda je retezec podposloupnosti vstupnich retezcu ze vstupni mnoziny
		
		// kontrola zda je retezec podposloupnosti vstupnich retezcu z mnoziny
		subSequence = checkAllStrings(inputStrings, candidate, N);

		// pokud je delka potencionaliho reseni vetsi nez K a zaroven vetsi nez delka dosud nalezeneho reseni
		if (candidate.length() >= K && candidate.length() > result.length() && subSequence) {
			result = candidate;
		}

		// pokud je delka nalezeneho reseni stejne dlouha, jako maximalni delka retezce, vypocet konci(je nalezeno reseni z mnoziny nejlepsich reseni) 
		if (result.length() == Z - 1) {
			return result;
		}

		// pokud je auktualni retezec podposloupnosti ostatnich, pokracuje se ve stromu o uroven nize
		if (subSequence && candidate.length() < (Z - 1)) {
			st.push(State("0", item.getIndex() + 1));
			st.push(State("1", item.getIndex() + 1));
		}
	}

	return result;
}

void printDecomposition(string * &inputStrings, const int &N, const string &result, ofstream &out) {
	for (int i = 0; i < N; i++) {
		string item = inputStrings[i];
		int index = 0;
		for (int j = 0; j < item.length(); j++) {
			if (item[j] == result[index]) {
				out << "(" << result[index] << ")";
				index++;
			} else {
				out << item[j];
			}
		}
		out << endl;
	}
}

/* Zapise vstupni data a vysledek do logu */
void printResult(string * &inputStrings, const int &N, const int &Z, const int &K, const string &result, const string &inputData, const uint64 &timeElapsed) {

	ofstream ofs;
	// otevreni streamu v modu append(neprepisuje soubor)
	ofs.open(LOG_FILE, ios::app);
	
	ofs << endl << "******************************************************************" << endl;
	ofs << "Soubor se vstupnimi daty: " << inputData << endl << endl;
	ofs << "N: " << N << endl;
	ofs << "Z: " << Z << endl;
	ofs << "K: " << K << endl << endl;

	if (result != "") {
		ofs << "Nejdelsi spolecna podposloupnost: " << result << endl << endl << "Rozklady: " << endl << endl;
		printDecomposition(inputStrings, N, result, ofs);
	} else {
		ofs << "Spolecna podposloupnost vyhovujici zadani neexistuje." << endl;
	}

	ofs << endl << "Delka trvani vypoctu(s): " << timeElapsed << endl;

	ofs.close();
}

int main(int argc, char * argv[]) {

	string inputData; // cesta k souboru se vstupnimi daty

	if (argv[1] != NULL) {
		inputData = argv[1];
	} else {
		cout << "Zadejte vstupni data jako prvni parametr programu." << endl;
		return 0;
	}

	string * inputStrings = NULL;
	string result = "";
	int N = 0, Z = 0, K = 0;

	if (!loadData(N, Z, K, inputStrings, inputData)) {
		if (inputStrings != NULL) {
			delete [] inputStrings;
		}
		return 1;
	}
	
	int expCtr = 0;

	startTime = GetTimeMs64();

	result = lcs(inputStrings, N, Z, K, expCtr);

	uint64 endTime = GetTimeMs64();

	uint64 timeElapsed = endTime - startTime;

	std::cout << "Provedeno " << expCtr << " expanzi." << std::endl;

	printResult(inputStrings, N, Z, K, result, inputData, timeElapsed / 1000);

	delete [] inputStrings;

	return 0;
}