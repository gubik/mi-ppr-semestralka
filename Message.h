#ifndef MESSAGE_H
#define MESSAGE_H

/*
 * Trida, ktera reprezentuje zpravu posilanou mezi procesory
 * Obsahuje vektor stavu(m_States) a jejich prefix(m_Prefix)
*/

class Message {
	std::vector<State> m_States;
	std::string m_Prefix;

public:
	Message(std::vector<State> states, const std::string &prefix) {
		m_States = states;
		m_Prefix = prefix;
	};

	Message() {
		m_Prefix = std::string("Hello");
	};

	std::vector<State> getStates() { 
		return m_States; 
	};

	std::string getPrefix() { 
		return m_Prefix; 
	};

	void addState(State state) {
		m_States.push_back(state);
	};

	void setPrefix(const std::string &prefix) { 
		m_Prefix = prefix; 
	};
	
protected:
	/* Serializace pro boost */
	friend class boost::serialization::access;

	template<class Archive>
	void serialize(Archive& ar, const unsigned int version) {
		ar & m_States & m_Prefix;
	};
};

#endif