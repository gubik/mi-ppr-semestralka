set terminal pngcairo font "1" enhanced
set output "zrychleni.png"
set encoding utf8
set title "Paralelní zrychlení na síti InfiniBand s různými datovými sadami"
set xlabel "Počet procesorů"
set xrange [0:34]
set xtics ("2" 2, "4" 4, "8" 8, "16" 16, "24" 24, "32" 32)
set ylabel "Čas [s]"

plot "zrychleni.dat" using 1:2 title "data61.txt" w linespoints, \
	 "zrychleni.dat" using 1:3 title "data62.txt" w linespoints, \
	 "zrychleni.dat" using 1:4 title "data64.txt" w linespoints, \
	 "zrychleni.dat" using 1:5 title "data65.txt" w linespoints, \
	 "zrychleni.dat" using 1:6 title "data68.txt" w linespoints
