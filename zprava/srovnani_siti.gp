set terminal pngcairo font "0.7" enhanced
set output "srovnani_siti.png"
set encoding utf8
set title "Porovnání parelelního zrychlení mezi sítěmi InfiniBand a Ethernet"
set xlabel "Počet procesorů"
set xrange [0:34]
set xtics ("2" 2, "4" 4, "8" 8, "16" 16, "24" 24, "32" 32)
set ylabel "Čas [s]"

plot "srovnani_siti.dat" using 1:2 title "InfiniBand - data61.txt" w linespoints, \
	 "srovnani_siti.dat" using 1:3 title "Ethernet - data61.txt" w linespoints, \
	 "srovnani_siti.dat" using 1:4 title "InfiniBand - data62.txt" w linespoints, \
	 "srovnani_siti.dat" using 1:5 title "Ethernet - data62.txt" w linespoints, \
	 "srovnani_siti.dat" using 1:6 title "InfiniBand - data64.txt" w linespoints, \
	 "srovnani_siti.dat" using 1:7 title "Ethernet - data64.txt" w linespoints, \
	 "srovnani_siti.dat" using 1:8 title "InfiniBand - data65.txt" w linespoints, \
	 "srovnani_siti.dat" using 1:9 title "Ethernet - data65.txt" w linespoints
