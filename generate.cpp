#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cmath>
#include <cstdlib>
#include <ctime>

using namespace std;

/* Vygeneruje nahodne N binarnich retezcu */
void generateStrings(string * &inputStrings, const int &N, const int &Z) {

	int stringLength; // delka jednoho retezce (log_2(N) < x < Z)
	srand(time(0)); // random seed
	
	for (int i = 0; i < N; i++) {

		// nahodna delka retezce
		stringLength = rand() % (Z - (int)ceil(log(N)/log(2)) - 1) + (int)ceil(log(N)/log(2)); 
		
		// reprezentuje jeden retezec
		string item;

		for (int j = 0; j < stringLength; j++) {
			// nahodne 0 nebo 1
			int bit = rand() % 2;
			stringstream ss;
			ss << bit; 
			item += ss.str();
		}

		// pripojeni retezce do pole
		inputStrings[i] = item;
	}
}

/* Vraci delku nejkratsiho retezce */
int getMinStringLength(string * &inputStrings, const int &N, const int &Z) {
	int min = Z;
	for (int i = 0; i < N; i++) {
		if (inputStrings[i].length() < min) {
			min = inputStrings[i].length();
		}
	}
	return min;
}

/* Vypis vsech retezcu */
void printStrings(string * &inputStrings, const int &N, ofstream &out) {

	for (int i = 0; i < N; i++) {
		out << inputStrings[i] << endl;
	}
}

/* Vytvori textovy soubor ve formatu pro vypocet nsp */
void createFile(string * &inputStrings, const int & N, const int & Z, const int & K, const string &filePath) {

	ofstream ofs;
	ofs.open(filePath.c_str());
	
	ofs << N << endl;
	ofs << Z << endl;

	printStrings(inputStrings, N, ofs);

	ofs << K;

	ofs.close();
}

int main() {
	int N = 0, Z = 0, K = 0, minLength = 0, lowerBound = 0;
	string filePath;

	while (N < 10) {
		cout << "Pocet retezcu(N >= 10): " << endl;
		cin >> N;
	}

	while (Z <= ceil((int)log(N)/log(2))) {
		cout << "Maximalni delka retezce(Z > log_2(N)): " << endl;
		cin >> Z;	
	}

	string * inputStrings;
	inputStrings = new string[N];

	generateStrings(inputStrings, N, Z);
	minLength = getMinStringLength(inputStrings, N, Z);

	while (K <= minLength / 2 || K >= minLength) {
		cout << "Minimalni delka spolecne podposloupnosti v intervalu(" << minLength/2 << ", " << minLength << "):" << endl;
		cin >> K;			
	}

	cout << "Cesta k souboru:" << endl;
	cin >> filePath;

	createFile(inputStrings, N, Z, K, filePath);

	delete [] inputStrings;
	return 0;
}