#ifndef PROCESS_H
#define PROCESS_H

#define MSG_CHECK_INTERVAL 	400
#define MSG_WORK_REQUEST 	1000
#define MSG_WORK_SENT    	1001
#define MSG_WORK_NOWORK  	1002
#define MSG_TARGET_REQUEST 	1003
#define MSG_TARGET_RESPONSE	1004
#define MSG_NEW_RESULT  	1005
#define MSG_BEST_RESULT		1006
#define MSG_TOKEN        	1007
#define MSG_FINISH       	1008

#define WHITE 0
#define BLACK 1

#define DEBUG 0
#define WORK_REQUEST_TARGET_SEARCH_TYPE 0 // 0 - lokalni cyklicke zadosti, 1 - globalni cyklicke zadosti 

/*
 * Trida, reprezentujici jeden proces
 */

class Process {
    /* Procesor 0 */
    /* flag signalizujici odeslani pocatecniho peska */
    bool isInitTokenSent;
    /* citac koncicich procesoru */
    int terminateCtr;

    /* cislo procesoru, ktery smi byt pozadan o praci */
    int workRequestTarget;

    /* Vsechny Procesory */
    /* vstupni retezce */
    std::string * inputStrings;
    /* parametry ulohy */
    int N;
    int Z;
    int K;
    /* rezna vyska */
    int H;

    /* zasobnik */
    std::stack<State> st;

    /* je odeslana zadost o praci? */
    bool isRequestSent;
    /* je procesor idle? */
    bool isIdle;
    /* barva peska, -1 pokud ho nema */
    int tokenColour;
    /* barva procesoru */
    int colour;
    /* flag signalizujici ukonceni vypoctu */
    bool terminate;

public:
    /* inicializace Boost MPI */
    boost::mpi::environment env;
    boost::mpi::communicator world;
    boost::mpi::timer timer;

    /* citac expanzi */
    int expCtr;

    Process() {
        /* procesor 0 */
        isInitTokenSent = false;
        terminateCtr = 0;

        if (WORK_REQUEST_TARGET_SEARCH_TYPE == 0) {
            workRequestTarget = (world.rank() + 1) % world.size();
        } else {
            workRequestTarget = 0;
        }

        /* vsechny procesory */
        N = 0;
        Z = 0;
        K = 0;
        isRequestSent = false;
        isIdle = false;
        tokenColour = -1;
        colour = WHITE;
        terminate = false;
        H = 7;
        expCtr = 0;
        if (world.rank() == 0) {
            std::cout << "Pocet procesoru: " << world.size() << std::endl;
        }
    };

    /* Zarizuje spravne nacteni vstupnich dat */
    bool loadData(char **argv) {
        std::string inputData; // cesta k souboru se vstupnimi daty

        if (argv[1] != NULL) {
            inputData = argv[1];
        } else {
            std::cerr << "Zadejte vstupni data jako prvni parametr programu." << std::endl;
            return false;
        }

        if (!parseInputFile(inputData)) {
            if (inputStrings != NULL) {
                delete [] inputStrings;
            }
            return false;
        }
        return true;
    };

    /* Zapise vstupni data a vysledek do logu */
    void printResult(const std::string &result, const char * logFile, const char * inputFile, const double &loadTime, const double &execTime) {

        std::ofstream ofs;
        /* otevreni streamu v modu append(neprepisuje soubor) */
        ofs.open(logFile, std::ios::app);

        ofs << std::endl << "******************************************************************" << std::endl;
        ofs << "Soubor se vstupnimi daty: " << inputFile << std::endl << std::endl;
        ofs << "Pocet procesoru: " << world.size() << std::endl;
        ofs << "Interval kontroly zprav: " << MSG_CHECK_INTERVAL << std::endl;
        ofs << "Rezna vyska: " << H << std::endl;
        ofs << "N: " << N << std::endl;
        ofs << "Z: " << Z << std::endl;
        ofs << "K: " << K << std::endl << std::endl;

        if (result != "") {
            ofs << "Nejdelsi spolecna podposloupnost: " << result << std::endl << std::endl << "Rozklady: " << std::endl << std::endl;
            printDecomposition(result, ofs);
        } else {
            ofs << "Spolecna podposloupnost vyhovujici zadani neexistuje." << std::endl;
        }

        ofs << std::endl << std::endl << "Doba nacteni dat(s): " << loadTime << std::endl;
        ofs << "Doba vypoctu(s): " << execTime << std::endl;
        ofs << "Celkova doba behu aplikace(s): " << loadTime + execTime << std::endl;

        ofs.close();
    }

    /* Zjisti nejdelsi spolecnou podposloupnost mnoziny retezcu */
    std::string parallelLCS() {
        std::string candidate = ""; // zpracovavany retezec v kazdem stavu
        std::string bestResult = ""; // dosud nejlepsi nalezene lokalni reseni

        bool initWorkDivision = false; // flag signalizujici pocatecni rozeslani dat procesorem 0

        if (world.rank() == 0) {
            /* procesor 0 naplni zasobnik pocatecnimi hodnotami */
            st.push(State("0", 0));
            st.push(State("1", 0));
        } else {
            /* ostatni cekaji na praci */
            Message msg;
            world.recv(0, MSG_WORK_SENT, msg);
            /* pridani pocatecniho stavu ziskaneho od procesoru 0 na zasobnik */
            st.push(msg.getStates()[0]);
            /* aktualizace prefixu */
            candidate = msg.getPrefix();
            if (DEBUG) std::cout << "[" << timer.elapsed() << "] Procesor: " << world.rank() << " Prijima pocatecni praci - Bit: " << msg.getStates()[0].getBit() << " na pozici: " << msg.getStates()[0].getIndex() << " s prefixem: " << msg.getPrefix() << std::endl;
        }

        while (!terminate) {
            isIdle = false;
            if (DEBUG) std::cout << "[" << timer.elapsed() << "] Procesor: " << world.rank() << " isIdle == FALSE ... pracuje" << std::endl;

            /* Dokud je nejaky stav na zasobniku */
            while (!st.empty()) {
                /* Inkrementace citace expanzi */
                
                /* po urcitem poctu expanzi obsluz frontu zprav */
                if ((expCtr % MSG_CHECK_INTERVAL) == 0) {
                    boost::optional<boost::mpi::status> status = world.iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG);
                    handleIncMessage(status, candidate, bestResult);
                }

                /* expanze stavu */
                expand(candidate, bestResult);
                /* inkrementace citace expanzi */
                expCtr++;

                /* pocatecni rozdeleni prace na ostatni procesory */
                if (world.rank() == 0 && !initWorkDivision && st.size() >= world.size()) {
                    for (int i = 1; i < world.size(); i++) {
                        State item = st.top();
                        st.pop();

                        /* Vytvoreni zpravy */
                        Message msg;
                        msg.addState(item);
                        msg.setPrefix(candidate);

                        /* odeslani zpravy s pocatecnim stavem procesoru i */
                        world.send(i, MSG_WORK_SENT, msg);
                    }
                    /* nastaveni flagu signalizujici hotove pocatecni rozdeleni */
                    initWorkDivision = true;
                }
            }

            /* Procesor se prave stal idle */
            isIdle = true;
            if (DEBUG) std::cout << "[" << timer.elapsed() << "] Procesor: " << world.rank() << " isIdle == TRUE ... ceka" << std::endl;


            /* procesor p0 posle pocatecniho peska */
            if (world.rank() == 0 && !isInitTokenSent) {
                /* procesor se obarvi na bilo */
                colour = WHITE;
                /* odeslani peska */
                world.send(1, MSG_TOKEN, WHITE);
                /* -1 = procesor nyni nema peska */
                tokenColour = -1;
                /* pocatecni pesek byl odeslan */
                isInitTokenSent = true;
                if (DEBUG) std::cout << "[" << timer.elapsed() << "] Procesor: 0 posila pocatecniho peska procesoru: 1" << std::endl;
            }

            /* Pokud mam peska, poslu ho dal */
            if (tokenColour != -1) {
                solveToken();
            }

            /* dokud je zasobnik prazdny nebo neni signalizovane ukonceni vypoctu */
            while (st.empty() && !terminate) {
                /* Zadna jina zadost neni v obehu */
                if (isRequestSent == false) {
                    /* Zjisti cislo procesoru, od ktereho ziskat praci */
                    int target = findWorkReqeustTarget();

                    /* Posli zadost o praci */
                    world.send(target, MSG_WORK_REQUEST, 0);
                    isRequestSent = true;
                }

                boost::optional<boost::mpi::status> status = world.iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG);
                handleIncMessage(status, candidate, bestResult);
            }
        }

        if (DEBUG) std::cout << "[" << timer.elapsed() << "] Procesor: " << world.rank() << " uz nema praci a ukoncuje vypocet." << std::endl;
        return bestResult;
    };

protected:

    /* Obslouzi prichozi zpravy */
    void handleIncMessage(boost::optional<boost::mpi::status> status, std::string &candidate, std::string &bestResult) {
        if (status) {
            switch (status->tag()) {
                case MSG_WORK_REQUEST:
                {
                    if (DEBUG) std::cout << "[" << timer.elapsed() << "] Procesor: " << world.rank() << " MSG_WORK_REQUEST" << std::endl;

                    /* Prisla zadost o praci, pokud mam dostatek stavu -> rozdelit, jinak poslat NO_WORK */
                    int dummy;
                    world.recv(status->source(), MSG_WORK_REQUEST, dummy);
                    if (st.size() > H) {
                        /* Mam dostatek stavu */
                        Message msg;

                        /* Rozdeleni zasobniku */
                        splitStack(msg, candidate);

                        /* odeslani zpravy */
                        world.send(status->source(), MSG_WORK_SENT, msg);

                        /* Pokud jsem poslal praci procesoru s mensim poradovym cislem nebo jsem 0, obarvim se na cerno*/
                        if (world.rank() > status->source()) {
                            colour = BLACK;
                        }

                        if (DEBUG) std::cout << "[" << timer.elapsed() << "] Procesor: " << world.rank() << " posila praci procesoru: " << status->source() << " Pocet poslanych stavu: " << msg.getStates().size() << " Pocet stavu na zasobniku: " << st.size() << std::endl;

                    } else {
                        /* Nemam dostatek stavu */
                        world.send(status->source(), MSG_WORK_NOWORK, 0);
                        if (DEBUG) std::cout << "[" << timer.elapsed() << "] Procesor: " << world.rank() << " Nedostatek stavu pro poslani prace procesoru: " << status->source() << std::endl;
                    }
                    break;
                }
                case MSG_WORK_SENT:
                {
                    if (DEBUG) std::cout << "[" << timer.elapsed() << "] Procesor: " << world.rank() << " MSG_WORK_SENT" << std::endl;

                    /* Prisla prace */
                    Message msg;
                    world.recv(status->source(), MSG_WORK_SENT, msg);

                    /* Prichozi vektor prekopiruju na zasobnik v opacnem poradi, poradi stavu na zasobniku bude stejne jako na zasobniku darce */
                    for (int i = msg.getStates().size() - 1; i != -1; i--) {
                        st.push(msg.getStates()[i]);
                    }
                    candidate = msg.getPrefix();
                    isRequestSent = false;
                    if (DEBUG) std::cout << "[" << timer.elapsed() << "] Procesor: " << world.rank() << " prijima praci od procesoru: " << status->source() << " Pocet poslanych stavu: " << msg.getStates().size() << std::endl;
                    break;
                }
                case MSG_WORK_NOWORK:
                {
                    if (DEBUG) std::cout << "[" << timer.elapsed() << "] Procesor: " << world.rank() << " MSG_WORK_NOWORK" << std::endl;

                    int dummy;
                    world.recv(MPI_ANY_SOURCE, MSG_WORK_NOWORK, dummy);

                    /* zjisti novy target a posli request */
                    int target = findWorkReqeustTarget();
                    world.send(target, MSG_WORK_REQUEST, 0);
                    break;
                }
                case MSG_TARGET_REQUEST:
                {
                    if (DEBUG) std::cout << "[" << timer.elapsed() << "] Procesor: " << world.rank() << " MSG_TARGET_REQUEST" << std::endl;

                    /* zadost o cislo procesoru, ktereho je mozne pozadat o praci */
                    int dummy;
                    world.recv(status->source(), MSG_TARGET_REQUEST, dummy);
                    if (status->source() == workRequestTarget) {
                        world.send(status->source(), MSG_TARGET_RESPONSE, (workRequestTarget + 1) % world.size());
                    } else {
                        world.send(status->source(), MSG_TARGET_RESPONSE, workRequestTarget);
                    }
                    workRequestTarget = (workRequestTarget + 1) % world.size();
                    break;
                }
                case MSG_NEW_RESULT:
                {
                    if (DEBUG) std::cout << "[" << timer.elapsed() << "] Procesor: " << world.rank() << " MSG_NEW_RESULT" << std::endl;

                    /* Prijem zpravy s novym nejlepsim resenim */
                    std::string recievedBest;
                    world.recv(MPI_ANY_SOURCE, MSG_NEW_RESULT, recievedBest);
                    /* Pokud je delka noveho reseni delsi nez stavajici, prepise se */
                    if (recievedBest.length() > bestResult.length()) {
                        bestResult = recievedBest;
                        if (DEBUG) std::cout << "[" << timer.elapsed() << "] Procesor: " << world.rank() << " prijima nove nejlepsi reseni: " << bestResult << " od procesoru: " << status->source() << std::endl;
                    }
                    break;
                }
                case MSG_BEST_RESULT:
                {
                    if (DEBUG) std::cout << "[" << timer.elapsed() << "] Procesor: " << world.rank() << " MSG_BEST_RESULT" << std::endl;

                    /* procesor 0 dostal zpravu o nalezeni nejlepsiho mozneho reseni, zahajuje ukonceni vypoctu */
                    world.recv(status->source(), MSG_BEST_RESULT, bestResult);
                    std::cout << "[" << timer.elapsed() << "] Procesor: " << status->source() << " nalezl nejlepsi mozne reseni." << std::endl;
                    for (int i = 1; i < world.size(); i++) {
                        world.send(i, MSG_FINISH, 0);
                        if (DEBUG) std::cout << "[" << timer.elapsed() << "] Procesor: " << world.rank() << " pos�l� MSG_FINISH 1 procesoru: " << i << std::endl;
                    }
                    break;
                }
                case MSG_TOKEN:
                {
                    /* Priejti peska */
                    if (DEBUG) std::cout << "[" << timer.elapsed() << "] Procesor: " << world.rank() << " MSG_TOKEN" << std::endl;

                    int recievedTokenColour;
                    world.recv(status->source(), MSG_TOKEN, recievedTokenColour);
                    tokenColour = recievedTokenColour;
                    if (DEBUG) std::cout << "[" << timer.elapsed() << "] Procesor: " << world.rank() << " (" << colour << ") prijima peska barvy: " << recievedTokenColour << " od procesoru: " << status->source() << " Pocet stavu na zasobniku: " << st.size() << std::endl;

                    solveToken();

                    break;
                }
                case MSG_FINISH:
                {
                    if (DEBUG) std::cout << "[" << timer.elapsed() << "] Procesor: " << world.rank() << " dostal MSG_FINISH od procesoru: " << status->source() << std::endl;

                    //konec vypoctu - proces 0 pomoci tokenu zjistil, ze jiz nikdo nema praci
                    //a rozeslal zpravu ukoncujici vypocet
                    //mam-li reseni, odeslu procesu 0
                    //nasledne ukoncim spoji cinnost
                    //jestlize se meri cas, nezapomen zavolat koncovou barieru MPI_Barrier (MPI_COMM_WORLD)
                    // return

                    if (world.rank() == 0) {
                        std::string recievedBest;
                        world.recv(MPI_ANY_SOURCE, MSG_FINISH, recievedBest);
                        if (recievedBest.length() >= bestResult.length()) {
                            bestResult = recievedBest;
                            if (DEBUG) std::cout << "[" << timer.elapsed() << "] Procesor: " << world.rank() << " prijima nove nejlepsi reseni: " << bestResult << " od procesoru: " << status->source() << std::endl;
                        }
                        terminateCtr++;
                        if (terminateCtr == world.size() - 1) {
                            terminate = true;
                        }
                        if (DEBUG) std::cout << "[" << timer.elapsed() << "] Procesor: " << world.rank() << " dostal potvrzeni od procesoru: " << status->source() << " o ukonceni vypoctu." << std::endl;
                    } else {
                        int dummy;
                        world.recv(0, MSG_FINISH, dummy);
                        if (DEBUG) std::cout << "[" << timer.elapsed() << "] Procesor: " << world.rank() << " dostal pokyn k ukonceni vypoctu. - nejlepsi lokalni reseni: " << bestResult << std::endl;
                        world.send(0, MSG_FINISH, bestResult);
                        terminate = true;
                    }
                    break;
                }
                default:
                    std::cerr << "Neznama zprava: " << status->tag() << std::endl;
                    break;
            }

        }
    };

    void solveToken() {
        /* posilam peska pouze pokud jsem idle */
        if (isIdle) {
            /* Procesor 1 */
            if (world.rank() == 0) {
                if (tokenColour == WHITE) {
                    /* Pokud procesor 0 prijal bileho peska a sam je bily, muze spustit ukonceni vypoctu */
                    if (DEBUG) std::cout << "[" << timer.elapsed() << "] Procesor: " << world.rank() << " (" << colour << ") obrzel bileho peska - Ukonceni vypoctu" << std::endl;
                    for (int i = 1; i < world.size(); i++) {
                        world.send(i, MSG_FINISH, 0);
                    }
                } else {
                    /* Jinak nastavi peska na bilo a zacne nove kolo */
                    if (DEBUG) std::cout << "[" << timer.elapsed() << "]" << " Dalsi kolo." << std::endl;
                    if (DEBUG) std::cout << "[" << timer.elapsed() << "] Procesor: " << world.rank() << " (" << colour << ") posila bileho peska procesoru: " << (world.rank() + 1) % world.size() << std::endl;
                    world.send((world.rank() + 1) % world.size(), MSG_TOKEN, WHITE);
                    colour = WHITE;
                    tokenColour = -1;
                }
                /* Ostatni procesory */
            } else {
                /* Ostatni procesory, pokud jsou idle mohou hned peska odeslat dale */
                if (colour == BLACK) {
                    world.send((world.rank() + 1) % world.size(), MSG_TOKEN, BLACK);
                    colour = WHITE;
                    tokenColour = -1;
                    if (DEBUG) std::cout << "[" << timer.elapsed() << "] Procesor: " << world.rank() << " (" << colour << ") posila peska barvy BLACK procesoru: " << (world.rank() + 1) % world.size() << std::endl;
                } else {
                    world.send((world.rank() + 1) % world.size(), MSG_TOKEN, tokenColour);
                    colour = WHITE;
                    if (DEBUG) std::cout << "[" << timer.elapsed() << "] Procesor: " << world.rank() << " (" << colour << ") posila peska barvy " << tokenColour << " procesoru: " << (world.rank() + 1) % world.size() << std::endl;
                    tokenColour = -1;
                }
            }
        }
    }

    int findWorkReqeustTarget() {
        int target;

        if (WORK_REQUEST_TARGET_SEARCH_TYPE == 1) {
            /* globalni cyklicke zadosti */
            if (world.rank() == 0) {
                if (workRequestTarget == 0) {
                    target = 1 % world.size();
                } else {
                    target = workRequestTarget;
                }
                workRequestTarget = (workRequestTarget + 1) % world.size();
            } else {
                world.send(0, MSG_TARGET_REQUEST, 0);
                world.recv(0, MSG_TARGET_RESPONSE, target);
            }
            if (DEBUG) std::cout << "[" << timer.elapsed() << "] Procesor: " << world.rank() << " zada o praci procesor: " << target << std::endl;
        } else {
            /* lokalni cyklicke zadost */
            if (workRequestTarget == world.rank()) {
                target = (workRequestTarget + 1) % world.size();
                workRequestTarget = (workRequestTarget + 1) % world.size();
            } else {
                target = workRequestTarget;
                workRequestTarget = (workRequestTarget + 2) % world.size();
            }
        }
        return target;
    }

    /* Konverze string -> int */
    int str2int(const std::string &str) {
        std::stringstream ss(str);
        int num;

        if ((ss >> num).fail()) {
            return -1;
        }
        return num;
    };

    /* Najde delku nejkratsiho retezce */
    int getMinStringLength() {
        int min = Z;
        for (int i = 0; i < N; i++) {
            if (inputStrings[i].length() < min) {
                min = inputStrings[i].length();
            }
        }
        return min;
    };

    /* Samotne nacteni vstupnich dat ze souboru */
    bool parseInputFile(const std::string &inputData) {
        std::string line; // jeden radek soubor
        int lineCtr = 0; // citac radku

        std::ifstream ifs;
        ifs.open(inputData.c_str());

        while (!ifs.eof()) {
            getline(ifs, line);

            if (lineCtr == 0) {
                // prvni radek (N)
                N = str2int(line);
                if (N < 10) {
                    std::cerr << "N nesmi byt mensi nez 10." << std::endl;
                    return false;
                }
                inputStrings = new std::string[N];
            } else if (lineCtr == 1) {
                // druhy radek (Z)
                Z = str2int(line);
                if (Z <= log(N) / log(2)) {
                    std::cerr << "Z musi byt vetsi nez log_2(" << N << ") = " << log(N) / log(2) << std::endl;
                    return false;
                }
            } else if (lineCtr == N + 2) {
                // posledni radek (K)
                K = str2int(line);
                int minLength = getMinStringLength();
                /* Nastaveni rezne vysky */
                //H = 2;
                if (K <= minLength / 2 || K >= minLength) {
                    std::cerr << "K musi byt v intervalu (" << minLength / 2 << ", " << minLength << ")." << std::endl;
                    return false;
                }
            } else {
                // naplneni pole retezcu
                inputStrings[lineCtr - 2] = line;
            }
            lineCtr++;
        }
        ifs.close();
        return true;
    };

    /* Kontroluje, zda je string podposloupnosti jineho stringu */
    bool isSubsequence(const std::string &subSequence, const std::string &input) {
        int pos = 0;

        for (int i = 0; i < input.length(); i++) {
            if (input[i] == subSequence[pos]) {
                pos++;
            }
            if (subSequence.length() == pos) {
                return true;
            }
        }
        return false;
    };

    /* Kontroluje, zda je string podposloupnosti vsech ostatnich */
    bool checkAllStrings(const std::string &subSequence) {
        for (int i = 0; i < N; i++) {
            if (!isSubsequence(subSequence, inputStrings[i])) {
                return false;
            }
        }
        return true;
    };

    void printDecomposition(const std::string &result, std::ofstream &out) {
        for (int i = 0; i < N; i++) {
            std::string item = inputStrings[i];
            int index = 0;
            for (int j = 0; j < item.length(); j++) {
                if (item[j] == result[index]) {
                    out << "(" << result[index] << ")";
                    index++;
                } else {
                    out << item[j];
                }
            }
            out << std::endl;
        }
    };

    void splitStack(Message &msg, const std::string &candidate) {
        /* K rozdeleni zasobniku se pouziva pomocny vektor */
        std::vector<State> tmp;
        if (st.size() >= 2 * H) {
            /* Na zasobniku je vice nez 2 * H prvku */
            /* Mohu rozdelit zasobnik na dve pulky */
            int halfStackSize = st.size() / 2;
            for (int i = 0; i < halfStackSize; i++) {
                State item = st.top();
                st.pop();
                tmp.push_back(item);
            }
        } else {
            /* Na zasobniku je mene nez 2 * H prvku */
            /* Ze zasobniku se vyjme H prvku, ty musi procesoru zustat */
            for (int i = 0; i < H; i++) {
                State item = st.top();
                st.pop();
                tmp.push_back(item);
            }
        }

        /* Zbytek stavu dostane jiny procesor */
        /* Nejprve predam prvni stav, podle ktereho musim urcit prefix pro rekonstrukci retezce */
        State item = st.top();
        st.pop();
        msg.addState(item);
        std::string prefix = candidate;
        prefix.resize(item.getIndex());
        msg.setPrefix(prefix);

        /* U ostatnich stavu se prefix resit nemusi */
        while (!st.empty()) {
            State item = st.top();
            st.pop();
            msg.addState(item);
        }
        /* Vratim obsah pomocneho vektoru zpet na zasobnik darce */
        for (int i = tmp.size() - 1; i != -1; i--) {
            st.push(tmp[i]);
        }
    };

    /* provede expanzi zasobniku */
    void expand(std::string &candidate, std::string &bestResult) {
        /* precti a odstran vrchol zasobniku */
        State item = st.top();
        st.pop();

        // orezani zpracovavaneho retezce podle pozice stavu ve stromu
        candidate.resize(item.getIndex());
        // pridani nasledujiciho bitu
        candidate += item.getBit();

        //if (DEBUG) std::cout << "Procesor: " << world.rank() << " testuje retezec: " << candidate << std::endl;

        bool subSequence = true; // zda je retezec podposloupnosti vstupnich retezcu ze vstupni mnoziny

        // kontrola zda je retezec podposloupnosti vstupnich retezcu z mnoziny
        subSequence = checkAllStrings(candidate);

        // pokud je delka potencionaliho reseni vetsi nez K a zaroven vetsi nez delka dosud nalezeneho reseni
        if (candidate.length() >= K && candidate.length() > bestResult.length() && subSequence) {
            bestResult = candidate;
            // pokud je delka nalezeneho reseni stejne dlouha, jako maximalni delka retezce, vypocet konci(je nalezeno reseni z mnoziny nejlepsich reseni) 
            if (bestResult.length() == Z - 1) {
                /* Odeslani zpravy o nejlepsim moznem reseni procesoru 0 */
                world.send(0, MSG_BEST_RESULT, bestResult);
                boost::optional<boost::mpi::status> status = world.probe(0, MSG_FINISH);
                handleIncMessage(status, candidate, bestResult);
                if (DEBUG) std::cout << "Procesor: " << world.rank() << "nalezl nejlepsiho mozne reseni: " << bestResult << std::endl;
            } else {
                /* Nalezene reseni je lepsi nez lokalni nejlepsi, odeslani zpravy vsem o nalezeni noveho nejlepsiho reseni */
                for (int i = 0; i < world.size(); i++) {
                    if (i == world.rank()) {
                        continue;
                    }
                    world.send(i, MSG_NEW_RESULT, bestResult);
                    if (DEBUG) std::cout << "[" << timer.elapsed() << "] Procesor: " << world.rank() << " posila nove reseni: " << bestResult << " procesoru: " << i << std::endl;
                }
            }
        }

        // pokud je aktualni retezec podposloupnosti ostatnich, pokracuje se ve stromu o uroven nize
        if (subSequence && candidate.length() < (Z - 1)) {
            st.push(State("0", item.getIndex() + 1));
            st.push(State("1", item.getIndex() + 1));
        }
    };
};

#endif